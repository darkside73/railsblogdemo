require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest
  
  test "should login user and redirect" do
    get login_path
    
    assert_response :success
    assert_template 'new'
    
    post session_path, :email => 'eugene@my.net', :password => 'secret'
    
    assert_response :redirect
    assert_redirected_to root_path
    
    follow_redirect!
    
    assert_response :success
    assert_template 'index'
    assert session[:user_id]
  end
  
  test "should logout user and redirect" do
    get logout_path
    
    assert_response :redirect
    assert_redirected_to root_path
    
    follow_redirect!
    
    assert_template 'index'
  end
  
  test "should login and create article" do
    
    # Login
    get login_path     
    post session_path, :email => 'eugene@my.net', :password => 'secret'    
    follow_redirect!
    
    # Create article
    get new_article_path
    
    assert_response :success
    assert_template 'new'
    
    post articles_path, :article => { :body => 'Test', :title => 'Test' }
    
    assert_response :redirect
    assert_redirected_to article_path(assigns(:article))
    
    follow_redirect!
    
    assert_response :success
    assert_template 'show'
  end
end
