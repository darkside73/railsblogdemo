class Notifier < ActionMailer::Base
  default from: "from@example.com"
  append_view_path "#{Rails.root}/app/views/mailers"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.email_friend.subject
  #
  def email_friend(article, sender_name, reciever_email)
    @article, @sender_name = article, sender_name
    mail :to => reciever_email, :subject => 'Interesting Article'
  end
  
  def comment_added(comment)
    @article = comment.article
    mail :to => @article.user.email, :subject => "New comment for '#{@article.title}'"
  end
end
