require 'digest'

class User < ActiveRecord::Base
  attr_accessible :email, :password, :password_confirmation
  
  attr_accessor :password
  
  validates :email, :presence => true,
            :uniqueness => true,
            :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
                  
  validates :password, :confirmation => true,
            :length => { :within => 4..20},
            :presence => true,
            :if => :password_required?
  validates :password_confirmation, :presence => true, :if => :password_required?
            
  
  has_one :profile
  has_many :articles, :order => "published_at DESC",
                      :dependent => :destroy
                      # or :dependent => :nullify
  has_many :replies, :through => :articles, :source => :comments
  
  before_save :encrypt_new_password
  
  def self.authenticate(email, password)
    user = find_by_email email
    return user if user && user.authenticated?(password)
  end
  
  def authenticated?(password)
    hashed_password == encrypt(password)
  end
  
  protected
    def password_required?
      hashed_password.blank? || password.present?
    end
  
    def encrypt_new_password
      self.hashed_password = encrypt(password) unless password.blank?
    end
    
    def encrypt(string)
      Digest::SHA1.hexdigest string
    end
end
